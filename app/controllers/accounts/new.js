import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    selectCurrency(currency) {
      this.set('model.currency', currency);
    }
  }
});
