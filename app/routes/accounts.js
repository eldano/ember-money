import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    return this.store.filter('account', {}, function(account) {
      return !account.get('isNew');
    });
  }
});
