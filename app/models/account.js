import DS from 'ember-data';

let attr = DS.attr;

export default DS.Model.extend({
  name: attr('string'),
  currency: attr('string', { defaultValue: 'USD' }),
  balance: attr('string'),
  starting_balance: attr('string', { defaultValue: '0.00' }),
  account_type: attr('string', { defaultValue: 'bank' })
});
