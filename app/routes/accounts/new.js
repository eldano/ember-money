import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('account');
  },

  actions: {
    save() {
      this.currentModel.save();
      this.transitionTo('accounts');
    },

    close() {
      this.currentModel.deleteRecord();
      this.transitionTo('accounts');
    }
  }
});
